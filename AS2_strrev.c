/*
 ============================================================================
 Name        : AS2.c
 Author      : SAURABH GAJENDRA
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include<stdio.h>
#include<string.h>

int main()
{
   char str[50] = "SUNBEAM";

   printf("The given string is =%s\n",str);

   printf("After reversing string is =%s",strrev(str));

   return 0;
}
