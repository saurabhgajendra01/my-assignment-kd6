/*
 ============================================================================
 Name        : src.c
 Author      : SAURABH GAJENDRA
 Version     :
 Copyright   : Your copyright notice
 Description : strcpy, Ansi-style
 ============================================================================
 */

   #include <stdio.h>
   #include <string.h>

   int main()
   {
     char s1[100],
          s2[100];
     strcpy( s1, "sunbeam karad" );
     strcpy( s2, " sunbeam pune " );

     puts( "Original strings: " );
     puts( "" );
     puts( s1 );
     puts( s2 );
     puts( "" );

     strcpy( s2, s1 );

     puts( "New strings: " );
     puts( "" );
     puts( s1 );
     puts( s2 );
   }
