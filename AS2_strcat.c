/*
 ============================================================================
 Name        : StringComparison.c
 Author      : SAURABH GAJENDRA
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */
#include <stdio.h>
#include <string.h>

int main () {
   char str1[50], str2[50];

   //destination string
   strcpy(str1, "initial string");

   //source string
   strcpy(str2, ", add this");

   //concatenating the string str2 to the string str1
   strcat(str1, str2);

   //displaying destination string
   printf("String after concatenation: %s", str1);

   return(0);
}
